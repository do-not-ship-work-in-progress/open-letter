+++
title = "Sign the open letter"
description = "Instructions on how to sign the pen letter"
+++

If you agree to our [open letter](@/_index.md), you might want to express your support by signing it too. This is possible and easy!

All signatures are in the file [signatures.csv](https://gitlab.com/do-not-ship-work-in-progress/open-letter/-/blob/main/signatures.csv) which has the following format:
```
Name,Affiliation,OptionalURL
```

You can sign the letter by opening a merge
request [in our repository](https://gitlab.com/do-not-ship-work-in-progress/open-letter) by adding yourself to the end of `signatures.csv`. Please use two comma separators on each line even if you don't specify an URL.

We will frequently review merge requests and add you to the letter.

**Thank you for your support and happy dev'ing!**
